############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import logging
import time

from abc import (ABCMeta, abstractmethod)
from sklearn.cluster import KMeans
from kernel_approximation import Nystroem


class SampleQuantizer(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def select_landmarks(self, X, num_landmarks, params=None):
        pass


class UniformSQ(SampleQuantizer):

    def __init__(self):
        self.logger = logging.getLogger('UniformSQ')

    """
        A set of landmarks is selected by taking a uniform subsample from the available instances (without replacement).

        Parameters:

            X : numpy array
                shape=(n, d), where n is the number of instances and d is the dimension of the problem

            num_landmarks : int

            params : [optional] dictionary

        Output:

            landmarks : numpy array
                        shape=(m, d), where m is the number of landmarks and d is the dimension of the problem
    """
    def select_landmarks(self, X, num_landmarks, params=None):
        start_t = time.time()

        landmark_ids = np.random.choice(X.shape[0], size=num_landmarks, replace=False)

        self.logger.debug('[Uniform Sample Quantization] TIME: ' + str(time.time() - start_t))

        return X[landmark_ids]


class KMeansSQ(SampleQuantizer):

    def __init__(self):
        self.logger = logging.getLogger('KMeansSQ')

    """
        A set of landmarks is selected by performing K-means clustering in the input space. In the first step, clusters are seeded using
        the K-means++ sampling scheme. Following this, the sampled cluster centroids are refined using the Lloyd's iterations.

        Parameters:

            X : numpy array
                shape=(n, d), where n is the number of instances and d is the dimension of the problem

            num_landmarks : int

            params : [optional] dictionary
                     tolerance : float
                                 stopping criteria for the Lloyd refinements in K-means clustering
                     max_iterations : int
                                      max number of Lloyd refinements
                     num_restarts : int
                                    number of restarts (i.e., different cluster seeding)

        Output:

            landmarks : numpy array
                        shape=(m, d), where m is the number of landmarks and d is the dimension of the problem
    """
    def select_landmarks(self, X, num_landmarks, params={'tolerance': 1e-2, 'max_iterations': 1000, 'num_restarts': 1}):
        start_t = time.time()

        kmeans = KMeans(n_clusters=num_landmarks, n_init=params['num_restarts'], max_iter=params['max_iterations'], tol=params['tolerance'])
        kmeans = kmeans.fit(X)

        self.logger.debug('[KMeans Sample Quantization] TIME: ' + str(time.time() - start_t))

        return kmeans.cluster_centers_


class KernelKMeansSQ(SampleQuantizer):

    def __init__(self, sklearn_kernel_function, kernel_params):
        self.logger = logging.getLogger('KernelKMeansSQ')
        self.sklearn_kernel_function = sklearn_kernel_function
        self.kernel_params = kernel_params

    def __sample_cluster_centers(self, X, kmat_diagonal, cp_distances, cp_potential, num_local_trials):
        rand_vals = np.random.rand(num_local_trials) * cp_potential
        candidate_ids = np.unique(np.searchsorted(cp_distances.cumsum(), rand_vals))
        distance_to_candidates = kmat_diagonal[candidate_ids].reshape(-1, 1) + kmat_diagonal.reshape(1, -1) - \
                                 2 * self.sklearn_kernel_function(X[candidate_ids], X, gamma=self.kernel_params['gamma'])

        best_candidate, best_cp_distances, best_cp_potential = None, None, None
        for trial, candidate in enumerate(candidate_ids):
            candidate_cp_distances = np.minimum(cp_distances, distance_to_candidates[trial])
            candidate_potential = candidate_cp_distances.sum()
            if (best_candidate is None) or (candidate_potential < best_cp_potential):
                best_candidate = candidate
                best_cp_potential = candidate_potential
                best_cp_distances = np.copy(candidate_cp_distances)

        return best_candidate, best_cp_distances, best_cp_potential

    """
        A set of landmarks is sampled using the kernel K-means++ sampling scheme (without Lloyd refinements).

        Parameters:

            X : numpy array
                shape=(n, d), where n is the number of instances and d is the dimension of the problem

            num_landmarks : int

            params : [optional] dictionary
                     num_local_trials : int
                                        number of trials for each centroid (except the first) -- among these the one with the largest
                                        reduction in the clustering potential is selected as the next sample (Arthur & Vassilvitskii, 2007)

        Output:

            landmarks : numpy array
                        shape=(m, d), where m is the number of landmarks and d is the dimension of the problem
    """
    def select_landmarks(self, X, num_landmarks, params={'num_local_trials': None}):
        start_t = time.time()

        n = X.shape[0]
        if params['num_local_trials'] is None:
            num_local_trials = 2 + int(np.log(num_landmarks))
        else:
            num_local_trials = params['num_local_trials']

        # TODO: pre-compute diagonal for kernels other than Gaussian
        kmat_diagonal = np.ones(n)

        init_landmark_id = np.random.randint(n)
        selected_landmarks = [init_landmark_id]

        cp_dists = kmat_diagonal[init_landmark_id] + kmat_diagonal - \
                   2 * self.sklearn_kernel_function(X[init_landmark_id].reshape(1, -1), X, gamma=self.kernel_params['gamma'])
        cp_potential = cp_dists.sum()

        for i in range(1, num_landmarks):
            landmark_id, cp_dists, cp_potential = self.__sample_cluster_centers(X, kmat_diagonal, cp_dists, cp_potential, num_local_trials)
            selected_landmarks.append(landmark_id)

        self.logger.debug('[Kernel K-Means++ Sample Quantization] TIME: ' + str(time.time() - start_t))

        return X[selected_landmarks]


class ApproximateLeverageScoresSQ(SampleQuantizer):

    def __init__(self, sklearn_kernel_function, kernel_params, max_sketch_sz=5000):
        self.logger = logging.getLogger('ApproximateLeverageScoresSQ')
        self.kernel_function = sklearn_kernel_function
        self.kernel_params = kernel_params
        self.max_sketch_sz = max_sketch_sz

    """
        A set of landmarks is selected by sampling instances proportional to the corresponding approximate leverage scores. The approximate
        leverage scores are computed using a sketch matrix.

        Parameters:

            X : numpy array
                shape=(n, d), where n is the number of instances and d is the dimension of the problem

            num_landmarks : int

            params : [optional] dictionary
                     sketch_sz : int
                                 the size of a sketch matrix
                     p : numpy array
                         shape=(n,), where n is the number of instances
                         (sketch matrix is created by sampling instances with probability distribution given by vector p)

        Output:

            landmarks : numpy array
                        shape=(m, d), where m is the number of landmarks and d is the dimension of the problem
    """
    def select_landmarks(self, X, num_landmarks, params={'sketch_sz': None, 'p': None}):
        start_t = time.time()

        n = X.shape[0]
        if params.get('sketch_sz', None) is None:
            sketch_sz = int(min(num_landmarks * np.log(n), self.max_sketch_sz))
        else:
            sketch_sz = params['sketch_sz']
        self.logger.debug('sketch size in leverage scores approximation: ' + str(sketch_sz))

        sketch_landmark_ids = np.random.choice(n, size=sketch_sz, replace=False, p=params.get('p', None))
        nys = Nystroem(self.kernel_function, self.kernel_params)
        S = nys.transform(X, X[sketch_landmark_ids])

        eig_vals, eig_vecs = np.linalg.eigh(S.T.dot(S))
        sqrt_top_eig_vals = np.sqrt(np.maximum(eig_vals[-num_landmarks:], 1e-12))
        U = S.dot(np.divide(eig_vecs[:, -num_landmarks:], sqrt_top_eig_vals))
        leverage_scores = np.linalg.norm(U, axis=1) ** 2

        landmark_ids = np.random.choice(n, size=num_landmarks, replace=False, p=leverage_scores / np.sum(leverage_scores))
        self.logger.debug('[Approximate Leverage Scores Sample Quantization] TIME: ' + str(time.time() - start_t))

        return X[landmark_ids]
