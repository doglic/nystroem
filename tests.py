############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import unittest as ut
import logging
import experiments as ex
import matplotlib.pyplot as pp
import sample_quantization as sq
import kernel_approximation as ka

from sklearn.metrics.pairwise import rbf_kernel
# from sklearn.kernel_approximation import Nystroem


class NystroemSQTests(ut.TestCase):

    def setUp(self):
        self.logger = logging.getLogger('NystroemSQTests')
        ex.start_loggers()

        self.bullseye_sz, self.bullseye_dim = 2000, 2

        u = np.random.standard_normal(size=(self.bullseye_sz, self.bullseye_dim))
        unorm = np.linalg.norm(u, axis=1).reshape(-1, 1)
        perturbation = np.random.rand(u.shape[0], 1)

        x1 = (u / unorm) * (.1 * perturbation + .9)
        x2 = (u / unorm) * (.5 * perturbation)

        self.x = np.append(x1, x2, axis=0)
        self.num_landmarks = 100

        self.params = {'tolerance': 1e-4, 'max_iterations': 1000}

        # self.x = self.x - np.mean(self.x, axis=0)
        # self.x /= np.std(self.x, axis=0)

    def test_kmneans_bullseye_quantization(self):
        kmeans_sq = sq.KMeansSQ()
        self.params['num_restarts'] = 1
        z = kmeans_sq.select_landmarks(self.x, self.num_landmarks, params=self.params)
        pp.scatter(self.x[:, 0], self.x[:, 1])
        pp.scatter(z[:, 0], z[:, 1], color='r')
        pp.show()

    def test_nystroem_approximation_error(self):
        self.params['gamma'] = 0.5
        kmat = rbf_kernel(self.x, gamma=self.params['gamma'])
        nystroem = ka.Nystroem(sklearn_kernel_function=rbf_kernel, params={'gamma': self.params['gamma']})

        pp.scatter(self.x[:, 0], self.x[:, 1], alpha=0.2)

        self.params['num_local_trials'] = None
        kernel_kmeanspp_sq = sq.KernelKMeansSQ(sklearn_kernel_function=rbf_kernel, kernel_params={'gamma': self.params['gamma']})
        z = kernel_kmeanspp_sq.select_landmarks(self.x, self.num_landmarks, params=self.params)
        pp.scatter(z[:, 0], z[:, 1], color='r')

        error = nystroem.approximation_error(self.x, z, z.shape[0], kmat, norm='frobenius')
        self.logger.info('[kernel K-means++ sampling scheme] approximation error in the Frobenius norm: ' + str(error))

        self.params['num_restarts'] = 1
        kmeans_sq = sq.KMeansSQ()
        z = kmeans_sq.select_landmarks(self.x, self.num_landmarks, params=self.params)
        pp.scatter(z[:, 0], z[:, 1], color='g')

        error = nystroem.approximation_error(self.x, z, z.shape[0], kmat, norm='frobenius')
        self.logger.info('[K-means clustering] approximation error in the Frobenius norm: ' + str(error))

        pp.show()
