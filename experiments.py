#!/usr/bin/python

############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import pickle
import logging
import sys
import os
import time
import multiprocessing as mp
import ctypes as ct

from logging import handlers
from sklearn.metrics.pairwise import (rbf_kernel, pairwise_distances)
from sample_quantization import (UniformSQ, KMeansSQ, ApproximateLeverageScoresSQ, KernelKMeansSQ)
from kernel_approximation import Nystroem


logger_ids = ['Experiments', 'Nystroem', 'ApproximateLeverageScoresSQ', 'KMeansSQ', 'UniformSQ', 'KernelKMeansSQ', 'NystroemSQTests']
_logger = logging.getLogger('Experiments')


def configure_file_log_handler(fpath, level=logging.INFO, max_file_size=5000000, num_backups=10):
    handler = handlers.RotatingFileHandler(fpath, maxBytes=max_file_size, backupCount=num_backups)
    formatter = logging.Formatter('[%(asctime)s] --- [%(levelname)5s] --- [%(name)28s] --- %(message)s')
    handler.setFormatter(formatter)
    handler.setLevel(level)
    return handler


def configure_logger(logger_id, log_fpath=None, log_level=logging.INFO, max_file_size=5000000, num_backups=10):
    logger = logging.getLogger(logger_id)
    logger.setLevel(log_level)

    if log_fpath is None:
        logger.addHandler(logging.StreamHandler())
    else:
        logger.addHandler(configure_file_log_handler(log_fpath, log_level, max_file_size, num_backups))


def start_loggers(log_fpath=None, log_level=logging.INFO):
    for logger_id in logger_ids:
        configure_logger(logger_id, log_fpath=log_fpath, log_level=log_level)


def load_data(fpath):
    f = open(fpath)
    x, y = pickle.load(f)
    f.close()
    return x, y


def uniform_landmarks(x, num_landmarks):
    unif_sq = UniformSQ()
    return unif_sq.select_landmarks(x, num_landmarks)


def kmeans_landmarks(x, num_landmarks):
    kmeans_sq = KMeansSQ()
    params = {'tolerance': 1e-4, 'max_iterations': 1000, 'num_restarts': 1}
    return kmeans_sq.select_landmarks(x, num_landmarks, params=params)


def kernel_kmeanspp_landmarks(x, num_landmarks, kernel_params, num_local_trials=None):
    kernel_kmeanspp_sq = KernelKMeansSQ(sklearn_kernel_function=rbf_kernel, kernel_params=kernel_params)
    return kernel_kmeanspp_sq.select_landmarks(x, num_landmarks, params={'num_local_trials': num_local_trials})


def leverage_scores_landmarks(x, num_landmarks, kernel_params, sketch_pdist=None):
    leverage_scores_sq = ApproximateLeverageScoresSQ(rbf_kernel, kernel_params)
    return leverage_scores_sq.select_landmarks(x, num_landmarks, params={'sketch_sz': num_landmarks, 'p': sketch_pdist})


supported_methods = ['uniform', 'kmeans', 'uniform_leverage_scores', 'kdiag_leverage_scores', 'kernel_kmeans++']


def mp_run_repetition(i, mp_dict, dim, gamma, num_landmarks, approx_rank, approx_norm_type):
    np_seed = int(int(time.time()) + (os.getpid() + (i + 1) * 997) * 997)  # 997 is a prime number
    np.random.seed(np_seed)

    nystroem = Nystroem(sklearn_kernel_function=rbf_kernel, params=__kernel_params)

    x = np.frombuffer(__mp_X).reshape(-1, dim)
    kmat = np.frombuffer(__mp_K).reshape(x.shape[0], -1)

    start_t = time.time()
    landmarks = uniform_landmarks(x, num_landmarks)
    mp_dict['time']['uniform'][gamma][i] = time.time() - start_t
    mp_dict['error']['uniform'][gamma][i] = nystroem.approximation_error(x, landmarks, approx_rank, kmat, norm=approx_norm_type)

    start_t = time.time()
    landmarks = kmeans_landmarks(x, num_landmarks)
    mp_dict['time']['kmeans'][gamma][i] = time.time() - start_t
    mp_dict['error']['kmeans'][gamma][i] = nystroem.approximation_error(x, landmarks, approx_rank, kmat, norm=approx_norm_type)

    start_t = time.time()
    landmarks = leverage_scores_landmarks(x, num_landmarks, __kernel_params)
    mp_dict['time']['uniform_leverage_scores'][gamma][i] = time.time() - start_t
    mp_dict['error']['uniform_leverage_scores'][gamma][i] = \
        nystroem.approximation_error(x, landmarks, approx_rank, kmat, norm=approx_norm_type)

    start_t = time.time()
    landmarks = kernel_kmeanspp_landmarks(x, num_landmarks, __kernel_params)
    mp_dict['time']['kernel_kmeans++'][gamma][i] = time.time() - start_t
    mp_dict['error']['kernel_kmeans++'][gamma][i] = \
        nystroem.approximation_error(x, landmarks, approx_rank, kmat, norm=approx_norm_type)

    start_t = time.time()
    sketch_pdist = np.copy(np.diag(kmat).reshape(-1))
    sketch_pdist /= np.sum(sketch_pdist)
    landmarks = leverage_scores_landmarks(x, num_landmarks, __kernel_params, sketch_pdist=sketch_pdist)
    mp_dict['time']['kdiag_leverage_scores'][gamma][i] = time.time() - start_t
    mp_dict['error']['kdiag_leverage_scores'][gamma][i] = \
        nystroem.approximation_error(x, landmarks, approx_rank, kmat, norm=approx_norm_type)

    return mp_dict


def mp_pool_init(mp_X, mp_K, kernel_params):
    global __mp_X, __mp_K, __kernel_params
    __mp_X, __mp_K, __kernel_params = mp_X, mp_K, kernel_params


def low_rank_approximation_experiment(mp_x, dim, num_landmarks, approx_rank, n_cores, gamma_log_space, approx_norm_type, num_reps=10):
    _logger.info('number of landmarks: ' + str(num_landmarks) + '\n')
    _logger.info('****************************************************************************************************************\n')

    x = np.frombuffer(mp_x).reshape(-1, dim)

    mp_dictionary = dict()
    mp_dictionary['error'], mp_dictionary['time'] = dict(), dict()
    for meth in supported_methods:
        mp_dictionary['error'][meth], mp_dictionary['time'][meth] = dict(), dict()
        for gamma in gamma_log_space:
            mp_dictionary['error'][meth][gamma] = np.zeros(num_reps)
            mp_dictionary['time'][meth][gamma] = np.zeros(num_reps)

    n = x.shape[0]
    mp_K = mp.RawArray(ct.c_double, n ** 2)

    for gamma in gamma_log_space:
        K = np.frombuffer(mp_K).reshape(n, n)
        K[:] = rbf_kernel(x, gamma=gamma)
        kernel_params = {'gamma': gamma}

        _logger.info('low-rank approximation with gamma: ' + str(gamma) + '\n')

        async_results = []
        processor_pool = mp.Pool(n_cores, initializer=mp_pool_init, initargs=(mp_x, mp_K, kernel_params))
        for rep in range(num_reps):
            async_results.append(processor_pool.apply_async(mp_run_repetition,
                                                            (rep, mp_dictionary, dim, gamma, num_landmarks, approx_rank, approx_norm_type)))
        processor_pool.close()
        processor_pool.join()

        for rep in range(num_reps):
            repetition_result = async_results[rep].get()
            for type_key in mp_dictionary:
                for alg_key in mp_dictionary[type_key]:
                    mp_dictionary[type_key][alg_key][gamma][rep] = repetition_result[type_key][alg_key][gamma][rep]

        del async_results

        for alg in supported_methods:
            _logger.info('algorithm: ' + str(alg) + '\n')

            _logger.info('ERROR[' + alg + ']: ' + str(mp_dictionary['error'][alg][gamma]))
            _logger.info('approximation error mean: ' + str(np.mean(mp_dictionary['error'][alg][gamma])))
            _logger.info('approximation error std:  ' + str(np.std(mp_dictionary['error'][alg][gamma])) + '\n')

            _logger.info('TIME[' + alg + ']:  ' + str(mp_dictionary['time'][alg][gamma]))
            _logger.info('approximation time mean: ' + str(np.mean(mp_dictionary['time'][alg][gamma])))
            _logger.info('approximation time std:  ' + str(np.std(mp_dictionary['time'][alg][gamma])) + '\n')

        _logger.info('\n****************************************************************************************************************\n')

    del mp_K


"""
    Parameters:

        experiment : string
                     [log_n | log_k | k] -- the number of landmarks for rank k approximation (n is the number of instances)

        data_path : string
                    location of the pickled data file with (x, y), where x is the data represented with numpy array and y is a label vector

        approx_rank : int
                      rank of the approximation

        approx_error_norm : string
                            [frobenius | spectral]

        logger_path : [optional] string
                      location where the log-file will be stored
"""
if __name__ == '__main__':
    g_experiment = sys.argv[1]
    g_x = load_data(sys.argv[2])[0]
    g_approx_rank = int(sys.argv[3])
    g_approx_error_norm = sys.argv[4]
    if len(sys.argv) > 5:
        start_loggers(log_fpath=sys.argv[5])
    else:
        start_loggers()

    np.set_printoptions(linewidth=250, precision=6)
    _logger.info('********************************************************************************************************************\n')
    _logger.info('experiment: ' + g_experiment)
    _logger.info('approximation rank: ' + str(g_approx_rank))
    _logger.info('approximation error norm: ' + g_approx_error_norm)
    _logger.info('number of samples: ' + str(g_x.shape[0]))
    _logger.info('dimension of instance space: ' + str(g_x.shape[1]))

    # number of cores should be chosen by considering the size of the full kernel matrix (stored in memory for evaluation) and the machine
    g_n_cores = min(mp.cpu_count() - 1, 5)
    g_gamma_log_space_sz = 10
    # NOTE: memory consumption of the parallel evaluation can be high when the number of instances is large
    # (whole kernel matrix is stored for evaluation purposes)
    # max number of instances fixed to 25000 (i.e., data sets with more than 25000 instances will be sub-sampled)
    g_n, g_dim, g_max_row_num, g_preparation_block_sz = g_x.shape[0], g_x.shape[1], 25000, 5000

    g_index_space = np.arange(g_n)
    np.random.shuffle(g_index_space)
    g_preparation_indices = g_index_space[:g_preparation_block_sz]
    g_pwds = pairwise_distances(g_x[g_preparation_indices], n_jobs=g_n_cores).reshape(-1)
    g_gamma_log_space = np.logspace(np.log(0.5 / np.percentile(g_pwds, 99) ** 2), 0, g_gamma_log_space_sz)
    _logger.info('gamma parameters: ' + str(g_gamma_log_space))
    del g_pwds

    if g_n > g_max_row_num:
        np.random.shuffle(g_index_space)
        g_x = g_x[g_index_space[:g_max_row_num]]
        g_n = g_x.shape[0]
    _logger.info('number of active instances: ' + str(g_n))

    g_mp_x = mp.RawArray(ct.c_double, g_n * g_dim)
    tmp = np.frombuffer(g_mp_x).reshape(g_n, -1)
    tmp[:] = g_x
    del g_x

    if g_experiment == 'log_n':
        g_num_landmarks = int(min(g_approx_rank * np.log(g_n), g_preparation_block_sz))
        low_rank_approximation_experiment(g_mp_x, g_dim, g_num_landmarks, g_approx_rank, g_n_cores, g_gamma_log_space, g_approx_error_norm)
    elif g_experiment == 'log_k':
        g_num_landmarks = int(min(g_approx_rank * np.log(g_approx_rank), g_preparation_block_sz))
        low_rank_approximation_experiment(g_mp_x, g_dim, g_num_landmarks, g_approx_rank, g_n_cores, g_gamma_log_space, g_approx_error_norm)
    elif g_experiment == 'k':
        low_rank_approximation_experiment(g_mp_x, g_dim, g_approx_rank, g_approx_rank, g_n_cores, g_gamma_log_space, g_approx_error_norm)
    else:
        raise Exception('experiment is not supported!')
