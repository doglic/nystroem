############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import logging


class Nystroem(object):

    def __init__(self, sklearn_kernel_function, params):
        self.logger = logging.getLogger('Nystroem')
        self.kernel_function = sklearn_kernel_function
        self.params = params

        self.np_linalg_norm_type = {'frobenius': 'fro', 'spectral': 2}

    """
        The method computes a low-rank factorization of kernel matrix using the Nystroem approach, i.e., K ~= SS^t.

        Parameters:

            X : numpy array
                shape=(n, d), where n is the number of instances and d is the dimension of the problem

            landmarks : numpy array
                        shape=(m, d), where m is the number of landmarks and d is the dimension of the problem

        Output:

            S : numpy array
                shape=(n, m), where n is the number of instances and m is the number of landmarks
    """
    def transform(self, X, landmarks):
        L = self.kernel_function(landmarks, gamma=self.params['gamma'])
        eig_vals, eig_vecs = np.linalg.eigh(L)
        sqrt_eig_vals = np.sqrt(np.maximum(eig_vals, 1e-12))
        mixed_kmat_block = self.kernel_function(X, landmarks, gamma=self.params['gamma'])
        return mixed_kmat_block.dot(np.divide(eig_vecs, sqrt_eig_vals.reshape(1, -1)))

    """
        The method computes a low-rank factorization of kernel matrix using the one-shot Nystroem method where m landmarks are selected
        to make a rank k approximation of the kernel matrix (with m > k).

        Parameters:

            X : numpy array
                shape=(n, d), where n is the number of instances and d is the dimension of the problem

            landmarks : numpy array
                        shape=(m, d), where m is the number of landmarks and d is the dimension of the problem

            approximation_rank : int

        Output:

            S : numpy array
                shape=(n, k), where n is the number of instances and k is the rank of the approximation
                K ~= SS^t
    """
    def one_shot_transform(self, X, landmarks, approximation_rank):
        S = self.transform(X, landmarks)
        eig_vecs = np.linalg.eigh(S.T.dot(S))[1][:, -approximation_rank:]
        return S.dot(eig_vecs)

    """
        The method evaluates a landmark selection strategy.

        Parameters:

            X : numpy array
                shape=(n, d), where n is the number of instances and d is the dimension of the problem

            landmarks : numpy array
                        shape=(m, d), where m is the number of landmarks and d is the dimension of the problem

            approximation_rank : int

            K : numpy array
                shape=(n, n), where n is the number of instances

            norm : [optional: frobenius | spectral] string
                   type of the norm for the evaluation of the approximation error

        Output:

            error : float
    """
    def approximation_error(self, X, landmarks, approximation_rank, K, norm='frobenius'):
        S = self.one_shot_transform(X, landmarks, approximation_rank)
        err = np.linalg.norm(K - S.dot(S.T), ord=self.np_linalg_norm_type[norm])
        self.logger.debug('approximation error with selected landmarks: ' + str(err))
        return err
