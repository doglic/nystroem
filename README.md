### PUBLICATION ###
Nystr�m Method with Kernel K-means++ Samples as Landmarks @ International Conference on Machine Learning (ICML), Sydney 2017

### URL ###
http://proceedings.mlr.press/v70/oglic17a.html

### DEPENDENCIES ###
numpy 1.12.1
sklearn 0.18.1
logging 0.5.1.2

### ADMIN ###
dino.oglic [at] domain

domain = uni-bonn.de
